var URL;
var data;

function getLocation() {
	$('#weather-content').fadeIn();
    $.ajax({
            url: "https://geoip-db.com/jsonp",
            jsonpCallback: "callback",
            dataType: "jsonp",
            success: function( location ) {
                $('#city').html(location.city + ", " + location.country_name);
                URL = "https://api.darksky.net/forecast/820026a340f55e213d7d1def4c25bfa8/" + location.latitude + ",-" + location.longitude;
                $.getJSON(URL, function(json) {
                    data = json;
                    var d = new Date(json.currently.time * 1000);
                    var days = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
                    var suffix = d.getHours() >= 12 ? "PM":"AM"; 
                    var hours = ((d.getHours() + 11) % 12 + 1);

                    $('#time').html( days[d.getDay()] + " " + hours + ":" + d.getMinutes() + " " + suffix );
                    $('#weather-summary').html(json.currently.summary);
                    // $('#icon').html(json.currently.icon);
                    var iconValue = iconSelector(json.currently.icon);
                    document.getElementById('icon').className = iconValue;
                    $('#temperature').html( Math.round( (json.currently.temperature - 32) * 5/9 ) );
                    $('#precipitation').html("Precipitation: " + Math.round(json.currently.precipIntensity * 10000) + "%");
                    $('#humidity').html("Humidity: " + json.currently.humidity * 100 + "%");
                    $('#windSpeed').html("Wind: " + Math.round(json.currently.windSpeed * 0.44704) + " m/s" );
                });  
            }
        });
}

function convertUNIT(celsium) {
    var value = Math.round(data.currently.temperature);
    if (celsium == true) {
        value = Math.round( (value - 32) * 5/9 );
        $("#cels").attr('class', 'active');
        $("#fara").attr('class', '');
    } else {
        $("#fara").attr('class', 'active');
        $("#cels").attr('class', '');
    }
    $('#temperature').html(value);
}

function iconSelector(apiVal) {
    // clear-day, clear-night, rain, snow, sleet, wind, fog, cloudy, partly-cloudy-day, partly-cloudy-night, hail, thunderstorm, tornado
    var myMap = new Map();
    myMap.set('clear-day', 'wi wi-day-sunny');
    myMap.set('clear-night', 'wi wi-night-clear');
    myMap.set('rain', 'wi wi-rain');
    myMap.set('snow', 'wi wi-snow');
    myMap.set('sleet', 'wi wi-sleet');
    myMap.set('wind', 'wi wi-strong-wind');
    myMap.set('fog', 'wi wi-fog');
    myMap.set('cloudy', 'wi wi-cloudy');
    myMap.set('partly-cloudy-day', 'wi wi-cloudy');
    myMap.set('partly-cloudy-night', 'wi wi-night-partly-cloudy');
    myMap.set('hail', 'wi wi-hail');
    myMap.set('thunderstorm', 'wi wi-thunderstorm');
    myMap.set('tornado', 'wi wi-tornado');
    // console.log(myMap.get(apiVal));
    return myMap.get(apiVal);
}